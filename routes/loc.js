var express = require('express');
var router = express.Router();
const yelp = require('yelp-fusion');
const apiKey = 'api key here'
let searchRequest = {
  term: 'restaurants',
  location: '',
  price: '',
  open_now: 'true'
};
const client = yelp.client(apiKey)

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('loc', { result: 'locations' });

});

router.post('/data', function( req, res, next) {
  let locations, final
  searchRequest.location = req.body.zip
  
  client.search(searchRequest).then(ress=> {
    locations = ress.jsonBody.businesses
  
    let rando = Math.floor(Math.random() * Math.floor(locations.length))
    res.render('loc', 
      {
        name: locations[rando].name,
        imgUrl: locations[rando].image_url,
        url: locations[rando].url,
        phone: locations[rando].phone,
        rating: locations[rando].rating,
        price: locations[rando].price,
        address1: locations[rando].location.display_address[0],
        address2: locations[rando].location.display_address[1]
      });

  }).catch(e => {
    console.log(e);
  });
 
});

module.exports = router;
